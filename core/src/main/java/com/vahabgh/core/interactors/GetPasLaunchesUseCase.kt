package com.vahabgh.core.interactors

import com.vahabgh.core.data.PastLaunchRepository

class GetPasLaunchesUseCase(private val pastLaunchRepository: PastLaunchRepository) {

    suspend fun invoke(limit : Int,offset : Int) = pastLaunchRepository.getPastLaunches(limit,offset)

}