package com.vahabgh.core.interactors

import com.vahabgh.core.data.LaunchDetailRepository

class GetLaunchDetailByNameUseCase(private val launchDetailRepository: LaunchDetailRepository) {

    suspend fun invoke(name: String) = launchDetailRepository.getLaunchByName(name)

}