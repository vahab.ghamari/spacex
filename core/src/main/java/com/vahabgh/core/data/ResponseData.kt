package com.vahabgh.core.data

class ResponseData<T> private constructor(
    val data: T? = null,
    val error: Exception? = null,
    val state: State
) {

    fun fold(success: (T) -> Unit, failure: (Exception) -> Unit) {
        when {
            data != null -> success(data)
            error != null -> failure(error)
            else -> return
        }
    }

    companion object {
        fun <T> success(data: T): ResponseData<T> {
            return ResponseData(
                data,
                state = State.SUCCESS
            )
        }

        fun <T> error(error: Exception): ResponseData<T> {
            return ResponseData(
                null,
                error,
                State.ERROR
            )
        }
    }

    enum class State {
        SUCCESS,
        ERROR
    }
}