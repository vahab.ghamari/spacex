package com.vahabgh.core.data

class LaunchDetailRepository(private val launchDetailDataSource: LaunchDetailDataSource) {

    suspend fun getLaunchByName(name : String) = launchDetailDataSource.getLaunchesByName(name)
}