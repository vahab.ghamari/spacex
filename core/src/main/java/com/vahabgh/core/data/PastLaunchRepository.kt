package com.vahabgh.core.data

class PastLaunchRepository(private val pastLaunchDataSource : PastLaunchDataSource) {

    suspend fun getPastLaunches(limit : Int , offset : Int) = pastLaunchDataSource.getPastLaunches(limit,offset)

}