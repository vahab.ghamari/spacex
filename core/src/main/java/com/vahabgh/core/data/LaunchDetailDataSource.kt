package com.vahabgh.core.data

import com.vahabgh.core.domain.LaunchDetail
import kotlinx.coroutines.flow.Flow

interface LaunchDetailDataSource {

    suspend fun getLaunchesByName(name: String) : Flow<ResponseData<LaunchDetail>>

}