package com.vahabgh.core.data

import com.vahabgh.core.domain.Launch
import kotlinx.coroutines.flow.Flow

interface PastLaunchDataSource {

    suspend fun getPastLaunches(limit : Int,offset : Int) : Flow<ResponseData<List<Launch>>>


}