package com.vahabgh.core.domain


data class Rocket(
    val rocketId : String?,
    val rocketName : String?
)