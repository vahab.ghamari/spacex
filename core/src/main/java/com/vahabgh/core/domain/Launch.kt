package com.vahabgh.core.domain


data class Launch(
    val id : String,
    val flightNumber : Int,
    val missionName : String,
    val missionDate : String,
    val launchYear : String,
    val detail : String,
    val rocket: Rocket?,
    val links: Links?,
    val successful: Boolean
)

