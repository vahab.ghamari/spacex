package com.vahabgh.core.domain


data class Links(
    val smallMissionImage : String?,
    val largeMissionImage : String?,
    val wikiLink : String?,
    val youtubeLink : String?,
    val article : String?,
    val youtubeId : String?
)