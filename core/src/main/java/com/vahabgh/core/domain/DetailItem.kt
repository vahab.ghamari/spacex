package com.vahabgh.core.domain


interface DetailItem

data class LaunchDetail(
    val title: String,
    val videoId: String,
    val detailItems: List<DetailItem>
)

data class InfoItem(
    val icon: Int,
    val title: String,
    val subtitle: String
) : DetailItem

data class HeaderItem(val title: String,val date : String) : DetailItem

data class DescriptionItem(val description: String) : DetailItem




