package com.vahabgh.spacex.presentation.ui.launchedetail.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.vahabgh.core.domain.DescriptionItem
import com.vahabgh.core.domain.DetailItem
import com.vahabgh.core.domain.HeaderItem
import com.vahabgh.core.domain.InfoItem
import com.vahabgh.spacex.R

class LaunchDetailAdapter(private val items: List<DetailItem>) : RecyclerView.Adapter<DetailViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailViewHolder {
        return DetailViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                viewType,
                parent,
                false
            )
        )
    }

    override fun getItemViewType(position: Int): Int {
        return when (items[position]) {
            is InfoItem -> R.layout.view_holder_info_item
            is DescriptionItem -> R.layout.view_holder_description_item
            is HeaderItem -> R.layout.view_holder_header_item
            else -> throw IllegalArgumentException("No view type found for this data")
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: DetailViewHolder, position: Int) {
        holder.bind(items[position])
    }
}