package com.vahabgh.spacex.presentation.ui.base

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.vahabgh.spacex.BR


/**
 *
 * it is abstract base class to implement main behavior of MVVM pattern
 * it works with ViewModel and DataBinding
 *
 * all the viewModels should extends form [BaseViewModel] class
 *
 */
abstract class BaseFragment<VM : BaseViewModel, DB : ViewDataBinding> : Fragment() {

    abstract val layoutRes: Int

    abstract val viewModel: VM

    open var binding: DB? = null

    /**
     * all the observers of viewModel start observing here
     */
    abstract fun bindObservables()

    /**
     * setting initial configuration in fragment
     */
    abstract fun config()

    open fun initBinding() {
        binding?.apply {
            setVariable(BR.vm, viewModel)
            executePendingBindings()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, layoutRes, container, false)
        binding!!.lifecycleOwner = this
        initBinding()
        bindObservables()
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        config()
    }

}