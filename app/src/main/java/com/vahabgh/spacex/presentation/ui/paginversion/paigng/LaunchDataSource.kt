package com.vahabgh.spacex.presentation.ui.paginversion.paigng

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.vahabgh.core.domain.Launch
import com.vahabgh.spacex.framework.api.LaunchesService
import com.vahabgh.spacex.framework.pastlaunches.LaunchDataMapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class LaunchDataSource(
    private val launchesService: LaunchesService,
    private val compositeDisposable: CompositeDisposable,
    private val launchDataMapper: LaunchDataMapper
) : PageKeyedDataSource<Int, Launch>() {

    private val limit = 15

    val paginationStateResponse = MutableLiveData<PagingStateResponse>()

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Launch>
    ) {
        paginationStateResponse.postValue(PagingStateResponse(null, InitialPageLoading))
        compositeDisposable.add(
            launchesService.getSinglePastLaunches(limit, 0)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError {
                    paginationStateResponse.postValue(
                        PagingStateResponse(
                            { loadInitial(params, callback) },
                            InitialPageFailed(it as Exception)
                        )
                    )
                }
                .subscribe({ list ->
                    callback.onResult(launchDataMapper.map(list), null, limit)
                    paginationStateResponse.postValue(
                        PagingStateResponse(null, InitialPageLoaded)
                    )
                }, {
                    paginationStateResponse.postValue(
                        PagingStateResponse(
                            { loadInitial(params, callback) },
                            InitialPageFailed(it as Exception)
                        )
                    )
                })
        )

    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Launch>) {
        paginationStateResponse.postValue(PagingStateResponse(null, PaginatingState))
        compositeDisposable.add(
            launchesService.getSinglePastLaunches(limit, params.key)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError {
                    paginationStateResponse.postValue(
                        PagingStateResponse({
                            loadAfter(params, callback)
                        }, PagingError(it as Exception))
                    )
                }
                .subscribe({ list ->
                    if (list.size >= limit) {
                        callback.onResult(launchDataMapper.map(list), params.key + limit)
                        paginationStateResponse.postValue(
                            PagingStateResponse(null, PaginatingState)
                        )
                    } else {
                        paginationStateResponse.postValue(
                            PagingStateResponse(null, PagingDone)
                        )
                    }
                }, {
                    paginationStateResponse.postValue(
                        PagingStateResponse(
                            { loadAfter(params, callback) },
                            PagingError(it as Exception)
                        )
                    )
                })
        )
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Launch>) {

    }

}