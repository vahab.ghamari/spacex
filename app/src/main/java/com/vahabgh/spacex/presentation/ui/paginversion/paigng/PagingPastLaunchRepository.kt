package com.vahabgh.spacex.presentation.ui.paginversion.paigng

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.vahabgh.core.domain.Launch
import com.vahabgh.spacex.framework.pastlaunches.LaunchDataMapper
import com.vahabgh.spacex.framework.api.LaunchesService
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class PagingPastLaunchRepository @Inject constructor(private val launchesService: LaunchesService,
                                                     private val mapper: LaunchDataMapper
)  {


    lateinit var launchesPageList : LiveData<PagedList<Launch>>
    lateinit var launchDataSourceFactory: LaunchDataSourceFactory


    fun fetchPastLaunches(compositeDisposable: CompositeDisposable) : LiveData<PagedList<Launch>> {

        launchDataSourceFactory = LaunchDataSourceFactory(launchesService, compositeDisposable,mapper)

         val config = PagedList.Config.Builder()
             .setEnablePlaceholders(false)
             .setPageSize(10)
             .build()

        launchesPageList = LivePagedListBuilder(launchDataSourceFactory,config).build()
        return launchesPageList

    }


    fun getPagingState() : LiveData<PagingStateResponse> {
        return Transformations.switchMap<LaunchDataSource,PagingStateResponse>(
            launchDataSourceFactory.launchLiveDataSource,LaunchDataSource::paginationStateResponse
        )
    }



}