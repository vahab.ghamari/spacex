package com.vahabgh.spacex.presentation.ui.launchedetail

import android.content.Context
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.vahabgh.core.domain.Launch
import com.vahabgh.core.domain.LaunchDetail
import com.vahabgh.core.interactors.GetLaunchDetailByNameUseCase
import com.vahabgh.spacex.R
import com.vahabgh.spacex.framework.launchdetail.LaunchDetailInteractors
import com.vahabgh.spacex.presentation.ui.base.BaseViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.launch

class LaunchDetailViewModel @ViewModelInject constructor(
    private val launchDetailInteractors: LaunchDetailInteractors)
    : BaseViewModel() {

    private val _launch = MutableLiveData<LaunchDetail>()
    val launch: LiveData<LaunchDetail>
        get() = _launch

    fun getData(name : String) {
        viewModelScope.launch {
            launchDetailInteractors.getLaunchDetailByNameUseCase.invoke(name).single().fold({
                _launch.value = it
            },{
                showNoDataView()
            })
        }
    }

}