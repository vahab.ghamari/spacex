package com.vahabgh.spacex.presentation.ui.pastlaunches.list

import androidx.recyclerview.widget.RecyclerView
import com.vahabgh.core.domain.Launch
import com.vahabgh.spacex.BR
import com.vahabgh.spacex.databinding.ViewHolderLaunchItemBinding

class LaunchItemViewHolder constructor(
    val binding: ViewHolderLaunchItemBinding,
    private val onItemClick: (Launch?) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    init {
        itemView.setOnClickListener {
            onItemClick.invoke(binding.listItem)
        }
    }


    fun bind(launch: Launch?) {
        launch?.let {
            with(binding) {
                setVariable(BR.listItem,launch)
                executePendingBindings()
            }
        }
    }

}