package com.vahabgh.spacex.presentation.app

import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class HiltSpaceXApp : SpaceXApp()