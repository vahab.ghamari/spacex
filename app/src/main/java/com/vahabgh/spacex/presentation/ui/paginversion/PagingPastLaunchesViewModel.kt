package com.vahabgh.spacex.presentation.ui.paginversion

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.vahabgh.core.domain.Launch
import com.vahabgh.spacex.presentation.ui.base.BaseViewModel
import com.vahabgh.spacex.presentation.ui.paginversion.paigng.PagingPastLaunchRepository
import com.vahabgh.spacex.presentation.ui.paginversion.paigng.PagingState
import com.vahabgh.spacex.presentation.ui.paginversion.paigng.PagingStateResponse
import io.reactivex.disposables.CompositeDisposable

class PagingPastLaunchesViewModel @ViewModelInject constructor(
    val repository : PagingPastLaunchRepository)
    : BaseViewModel() {


    private val compositeDisposable = CompositeDisposable()

    val launchList : LiveData<PagedList<Launch>> by lazy {
        repository.fetchPastLaunches(compositeDisposable)
    }

    val networkState : LiveData<PagingStateResponse> by lazy {
        repository.getPagingState()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}