package com.vahabgh.spacex.presentation.ui.launchedetail

import androidx.annotation.NonNull
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.google.android.material.appbar.AppBarLayout
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.vahabgh.core.domain.DetailItem
import com.vahabgh.core.domain.LaunchDetail
import com.vahabgh.spacex.R
import com.vahabgh.spacex.databinding.FragmentLaunchDetailBinding
import com.vahabgh.spacex.presentation.ui.base.BaseFragment
import com.vahabgh.spacex.presentation.ui.launchedetail.list.LaunchDetailAdapter
import com.vahabgh.spacex.presentation.util.extentions.gone
import com.vahabgh.spacex.presentation.util.extentions.isNetworkConnected
import com.vahabgh.spacex.presentation.util.extentions.visible
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class LaunchDetailFragment : BaseFragment<LaunchDetailViewModel, FragmentLaunchDetailBinding>() {

    override val layoutRes: Int
        get() = R.layout.fragment_launch_detail

    override val viewModel: LaunchDetailViewModel by viewModels()

    private val args: LaunchDetailFragmentArgs by navArgs()

    private var youTubePlayer: YouTubePlayer? = null

    private var videoId = ""

    override fun bindObservables() {
        viewModel.launch.observe(viewLifecycleOwner, Observer {
            setData(it)
        })
    }

    private fun setData(launchDetail: LaunchDetail?) {
        launchDetail?.let {
            binding?.tvTitle?.text = (launchDetail.title)
            videoId = launchDetail.videoId
            setYoutubeVideo(videoId)
            setAdapter(it.detailItems)
        }
    }

    private fun setAdapter(detailItems: List<DetailItem>) {
        if (detailItems.isNotEmpty()) {
            binding?.rvRecycler?.adapter =
                LaunchDetailAdapter(detailItems)
        }
    }

    private fun setYoutubeVideo(youtubeId: String?) {
        if (isNetworkConnected()) {
            binding?.frError?.visible()
            binding?.youtubePlayerView?.let {
                lifecycle.addObserver(it)
                it.addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {
                    override fun onReady(@NonNull youTubePlayer: YouTubePlayer) {
                        this@LaunchDetailFragment.youTubePlayer = youTubePlayer
                        binding?.frError?.gone()
                        val videoId = youtubeId!!
                        youTubePlayer.loadVideo(videoId, 0f)
                    }
                })
            }
        } else {
            showVideoErrorView()
        }
    }

    private fun showVideoErrorView() {
        binding?.pbLoading?.gone()
        binding?.gTextButtonError?.visible()
        binding?.frError?.visible()
    }

    private fun hideVideoErrorView() {
        binding?.pbLoading?.visible()
        binding?.gTextButtonError?.gone()
    }

    override fun config() {
        initAppBar()
        setClickListener()
        viewModel.getData(args.launchName)
    }

    private fun setClickListener() {
        binding?.ivBack?.setOnClickListener {
            activity?.onBackPressed()
        }

        binding?.btnRetry?.setOnClickListener {
            hideVideoErrorView()
            setYoutubeVideo(videoId)
        }
    }

    private fun initAppBar() {

        forceTitleInvisible()

        binding?.appBar?.layoutParams.let {
            val height = (requireContext().resources.displayMetrics.widthPixels * 9) / 16.toFloat()
            it?.height = height.toInt()
            binding?.appBar?.layoutParams = it
        }

        binding?.appBar?.apply {
            this.layoutParams = layoutParams?.let {
                val height =
                    (requireContext().resources.displayMetrics.widthPixels * 9) / 16.toFloat()
                it.height = height.toInt()
                it
            }

            addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { _, offset ->
                animateHandler(offset)
            })
        }
    }

    private fun forceTitleInvisible() {
        val a = binding?.tvTitle?.animate() ?: return
        a.alpha(0f)
        a.duration = 0
        a.start()
    }

    private fun animateHandler(offset: Int) {
        binding?.appBar?.apply {
            if (height == 0) return
            val target = height / 2.toFloat()
            if (target + offset < 0)
                appBarIsClosed()
            else
                appBarIsOpen()
        }

    }

    private fun showTitle() {
        val a = binding?.tvTitle?.animate()?.alpha(1f)
        a?.duration = 200
        a?.start()
    }

    private fun hideTitle() {
        val a = binding?.tvTitle?.animate()?.alpha(0f)
        a?.duration = 200
        a?.start()
    }

    private fun appBarIsOpen() {
        hideTitle()
        resumeVideo()
    }

    private fun appBarIsClosed() {
        showTitle()
        stopVideo()
    }

    private fun resumeVideo() {
        youTubePlayer?.play()
    }

    private fun stopVideo() {
        youTubePlayer?.pause()
    }

}