package com.vahabgh.spacex.presentation.ui.pastlaunches.list

data class FooterLoading(var hasError : Boolean, var errMessage : String  = "")