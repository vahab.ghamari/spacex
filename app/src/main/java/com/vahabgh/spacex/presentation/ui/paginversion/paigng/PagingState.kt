package com.vahabgh.spacex.presentation.ui.paginversion.paigng

/**
 * this sealed class defines different state of pagination process
 *
 * [InitialPageLoading] start loading for first page
 * [InitialPageLoaded] first page is fetched
 * [InitialPageFailed] fetching first page failed
 * [PaginatingState] first page is loaded and pagination process is in progress
 * [PagingDone] pagination is done
 * [PagingError] some error happened in pagination error
 *
 */
sealed class PagingState

object InitialPageLoading : PagingState()

object InitialPageLoaded : PagingState()

data class InitialPageFailed(val exception: Exception) : PagingState()

object PaginatingState : PagingState()

object PagingDone : PagingState()

data class PagingError(val exception: Exception) : PagingState()


/**
 * this class wraps state of pagination state and a delegate for retry request
 * in case of error
 *
 * @param retry delegate for retry from UI
 * @param state pagination state
 *
 */
data class PagingStateResponse(
    val retry: (() -> Unit)?,
    val state: PagingState
)


