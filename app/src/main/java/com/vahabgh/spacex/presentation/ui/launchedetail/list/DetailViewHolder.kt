package com.vahabgh.spacex.presentation.ui.launchedetail.list

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.vahabgh.core.domain.DetailItem
import com.vahabgh.spacex.BR

open class DetailViewHolder(val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

    open fun bind(detailItem: DetailItem?) {
        detailItem?.let {
            with(binding){
                setVariable(BR.detailItem,detailItem)
                executePendingBindings()
            }
        }

    }

}