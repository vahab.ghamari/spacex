package com.vahabgh.spacex.presentation.util.extentions

import android.content.Context
import com.vahabgh.spacex.R
import com.vahabgh.spacex.presentation.util.network.ForbiddenException
import com.vahabgh.spacex.presentation.util.network.InternalServerException
import com.vahabgh.spacex.presentation.util.network.NoConnectivityException
import com.vahabgh.spacex.presentation.util.network.NotFoundException


/**
 * handling messages for my custom exceptions
 */
fun Exception.getMessage(context: Context): String {

    if (this is NoConnectivityException)
        return context.getString(R.string.space_x_connection_issue)

    if (this is InternalServerException ||
        this is NotFoundException ||
        this is ForbiddenException
    )
        return context.getString(R.string.space_x_server_issue)

    return context.getString(R.string.space_x_failed_operation_message)

}