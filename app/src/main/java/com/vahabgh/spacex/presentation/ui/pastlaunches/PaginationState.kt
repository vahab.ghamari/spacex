package com.vahabgh.spacex.presentation.ui.pastlaunches


/**
 * this sealed class is used for managing different state of pagination
 *
 * [PaginationDone] when the last page is fetched and pagination is over
 *
 * [Paginating] pagination is not over yet
 *
 * [PaginationError] fetching one page has some issue
 *
 */
sealed class PaginationState

object PaginationDone : PaginationState()

object Paginating : PaginationState()

object PaginationError : PaginationState()

