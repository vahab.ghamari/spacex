package com.vahabgh.spacex.presentation.ui.pastlaunches

import android.content.Context
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.vahabgh.core.domain.Launch
import com.vahabgh.spacex.R
import com.vahabgh.spacex.framework.pastlaunches.PastLaunchInteractors
import com.vahabgh.spacex.presentation.ui.base.BaseViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.launch

class PastLaunchesViewModel @ViewModelInject constructor(private val launchInteractors: PastLaunchInteractors) :
    BaseViewModel() {

    private val _pastLaunches = MutableLiveData<MutableList<Any>>()
    val pastLaunches: LiveData<MutableList<Any>>
        get() = _pastLaunches

    private val _paginateState = MutableLiveData<PaginationState>()
    val paginateState: LiveData<PaginationState>
        get() = _paginateState

    private val allData = mutableListOf<Any>()

    val limit = 15
    var offset = 0

    fun getFirstPage() {

        if (!allData.isNullOrEmpty()) {
            _pastLaunches.value = allData.filterIsInstance<Launch>().toMutableList()
            _paginateState.value = paginateState.value
            return
        }

        showProgress()
        viewModelScope.launch {
            launchInteractors.getPasLaunchesUseCase.invoke(limit, offset).single().fold({
                hideProgress()
                handleResponse(it)
            }, {
                hideProgress()
                showException(it)
            })
        }
    }

    fun getNextPage() {
        if (_paginateState.value == PaginationDone) return
        _paginateState.value = Paginating
        viewModelScope.launch {
            launchInteractors.getPasLaunchesUseCase.invoke(limit, offset).single().fold({
                handleResponse(it)
            }, {
                _paginateState.value = PaginationError
            })
        }
    }

    private fun handleResponse(data: List<Launch>) {
        if (data.isNullOrEmpty()) {
            if (isFirstPage())
                showNoDataView()
            else
                _paginateState.value = PaginationDone
        } else {
            hideErrorLoadingView()
            allData.addAll(data)
            _pastLaunches.value = data.toMutableList()
            if (isLastPage(data.size))
                _paginateState.value = PaginationDone
            else {
                offset += limit
                _paginateState.value = Paginating
            }
        }
    }

    private fun isFirstPage(): Boolean {
        return offset == 0
    }

    private fun isLastPage(listSize: Int): Boolean {
        return listSize < limit
    }

}