package com.vahabgh.spacex.presentation.ui.pastlaunches

import android.os.Parcelable
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.vahabgh.core.domain.Launch
import com.vahabgh.spacex.R
import com.vahabgh.spacex.databinding.FragmentPastLaunchesBinding
import com.vahabgh.spacex.presentation.ui.base.BaseFragment
import com.vahabgh.spacex.presentation.ui.pastlaunches.list.EndlessScrollListener
import com.vahabgh.spacex.presentation.ui.pastlaunches.list.PastLaunchesAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
open class PastLaunchesFragment : BaseFragment<PastLaunchesViewModel, FragmentPastLaunchesBinding>() {

    private var recyclerViewState: Parcelable? = null

    override val layoutRes: Int
        get() = R.layout.fragment_past_launches

    override val viewModel: PastLaunchesViewModel by viewModels()

    override fun bindObservables() {

        if (!viewModel.pastLaunches.hasObservers()) {
            viewModel.pastLaunches.observe(viewLifecycleOwner, Observer {
                setData(it)
            })
        }

        if (!viewModel.paginateState.hasObservers()) {
            viewModel.paginateState.observe(viewLifecycleOwner, Observer { pagingState ->
                handlePagingState(pagingState)
            })
        }

    }


    /**
     *it handles the pagination state which leads to show right view of footer item.
     */
    private fun handlePagingState(pagingState: PaginationState) {

        when (pagingState) {
            PaginationDone -> {
                                (binding?.rvPastLaunches?.adapter as?
                        PastLaunchesAdapter)
                    ?.removeFooter()
            }
            Paginating -> {
                                (binding?.rvPastLaunches?.adapter as?
                                        PastLaunchesAdapter)
                                    ?.addFooter()
            }
            PaginationError -> {
                                (binding?.rvPastLaunches?.adapter as?
                        PastLaunchesAdapter)
                    ?.showErrorFooter("")
            }
        }

    }

    private fun setData(it: MutableList<Any>) {
        if (binding?.rvPastLaunches?.adapter == null) {
            binding?.rvPastLaunches?.adapter = PastLaunchesAdapter(
                it, ::onItemClick, ::retryDelegate
            )
        } else (binding?.rvPastLaunches?.adapter as? PastLaunchesAdapter)?.updateData(it)

        restoreRecyclerViewStateIfExist()
    }

    override fun config() {

        binding?.tvPagingLibVersion?.setOnClickListener {
            PastLaunchesFragmentDirections
                .actionPasLaunchesFragmentToPagingPastLaunchesFragment().let {
                    findNavController().navigate(it)
                }
        }
        binding?.errorLoadingView?.setOnRetryClickListener {
            viewModel.getFirstPage()
        }
        binding?.rvPastLaunches?.addItemDecoration(DividerItemDecoration(context
            , LinearLayoutManager.VERTICAL))
        addOnScrollListener()

        viewModel.getFirstPage()
    }


    /**
     * adding listener for getting next page data at the end of the list
     */
    private fun addOnScrollListener() {
        binding?.rvPastLaunches?.addOnScrollListener(object : EndlessScrollListener() {
            override fun onLoadMore() {
                viewModel.getNextPage()
            }
        })

    }


    /**
     *
     * it is the delegate when an item is clicked in recyclerView
     * @param[launch] is the clicked item by user
     *
     */
    private fun onItemClick(launch: Launch?) {
        launch?.let { l ->
            PastLaunchesFragmentDirections
                .actionPasLaunchesFragmentToLaunchDetailFragment(l.missionName)
                .let {
                    findNavController().navigate(it)
                }
        }

    }


    /**
     * retry delegate for fetching next pages from
     * [com.vahabgh.spacex.presentation.ui.pastlaunches.list.FooterViewHolder]
     * when error happens
     */
    private fun retryDelegate() {
        (binding?.rvPastLaunches?.adapter as? PastLaunchesAdapter)?.hideFooterError()
        viewModel.getNextPage()
    }


    /**
     * save state of recycler in one parcelable[recyclerViewState] recyclerview for retrieving after configuration change
     * or destroying view
     */
    private fun saveRecyclerViewState() {
        recyclerViewState = binding?.rvPastLaunches?.layoutManager?.onSaveInstanceState()
    }

    /**
     * restoring state of recycler from parcelable[recyclerViewState] view for retrieving
     * former state of recyclerview
     */
    private fun restoreRecyclerViewStateIfExist() {
        recyclerViewState?.apply {
            binding?.rvPastLaunches
                ?.layoutManager
                ?.onRestoreInstanceState(this)
            recyclerViewState = null
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        saveRecyclerViewState()
    }
}