package com.vahabgh.spacex.presentation.ui.pastlaunches.list

import androidx.recyclerview.widget.RecyclerView
import com.vahabgh.spacex.BR
import com.vahabgh.spacex.databinding.ViewHolderFooterLoadingBinding
import com.vahabgh.spacex.presentation.ui.paginversion.paigng.PaginatingState
import com.vahabgh.spacex.presentation.ui.paginversion.paigng.PagingError
import com.vahabgh.spacex.presentation.ui.paginversion.paigng.PagingState
import com.vahabgh.spacex.presentation.util.extentions.getMessage
import java.lang.Exception

class FooterViewHolder(val binding : ViewHolderFooterLoadingBinding, private val retryDelegate : () -> Unit)
    : RecyclerView.ViewHolder(binding.root) {

    init {
        binding.btnRetry.setOnClickListener {
            retryDelegate.invoke()
        }
    }

    fun bind(footerLoading: FooterLoading?) {
        footerLoading?.let {
            with(binding){
                setVariable(BR.listItem,footerLoading)
                executePendingBindings()
            }
        }
    }

    fun bind(pagingState: PagingState?) {
        pagingState?.let {
            bind(getFooterDataByState(it))
        }
    }

    private fun getFooterDataByState(pagingState: PagingState?): FooterLoading? {
        return when(pagingState) {
            PaginatingState -> FooterLoading(false, "")
            is PagingError -> FooterLoading(true,
                pagingState.exception.getMessage(itemView.context))
            else -> null
        }
    }

}
