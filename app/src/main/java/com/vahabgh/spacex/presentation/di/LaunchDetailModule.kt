package com.vahabgh.spacex.presentation.di

import android.content.Context
import com.vahabgh.core.data.LaunchDetailDataSource
import com.vahabgh.core.data.LaunchDetailRepository
import com.vahabgh.core.interactors.GetLaunchDetailByNameUseCase
import com.vahabgh.spacex.framework.launchdetail.LaunchDetailDataSourceImpl
import com.vahabgh.spacex.framework.launchdetail.LaunchDetailInteractors
import com.vahabgh.spacex.framework.db.LaunchesDatabase
import com.vahabgh.spacex.framework.launchdetail.LaunchDetailDataMapper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ActivityRetainedScoped


@Module
@InstallIn(ActivityRetainedComponent::class)
object LaunchDetailModule {

    @Provides
    @ActivityRetainedScoped
    fun provideLaunchDetailDataMapper(@ApplicationContext context:Context): LaunchDetailDataMapper {
        return LaunchDetailDataMapper(context)
    }

    @Provides
    @ActivityRetainedScoped
    fun provideLaunchDetailDataSource(
        launchesDatabase: LaunchesDatabase,
        launchDetailDataMapper: LaunchDetailDataMapper
    ): LaunchDetailDataSource {
        return LaunchDetailDataSourceImpl(
            launchesDatabase,
            launchDetailDataMapper
        )
    }

    @Provides
    @ActivityRetainedScoped
    fun provideLaunchDetailRepository(launchDetailDataSource: LaunchDetailDataSource): LaunchDetailRepository {
        return LaunchDetailRepository(launchDetailDataSource)
    }

    @Provides
    @ActivityRetainedScoped
    fun provideGetLaunchDetailUseCase(launchDetailRepository: LaunchDetailRepository): GetLaunchDetailByNameUseCase {
        return GetLaunchDetailByNameUseCase(launchDetailRepository)
    }

    @Provides
    @ActivityRetainedScoped
    fun provideLaunchDetailInteractors(
        getLaunchDetailByNameUseCase: GetLaunchDetailByNameUseCase
    ): LaunchDetailInteractors {
        return LaunchDetailInteractors(
            getLaunchDetailByNameUseCase
        )
    }


}