package com.vahabgh.spacex.presentation.util.ba

import androidx.databinding.BindingAdapter
import com.vahabgh.spacex.R
import com.vahabgh.spacex.presentation.util.customview.ErrorLoadingView
import com.vahabgh.spacex.presentation.util.extentions.gone
import com.vahabgh.spacex.presentation.util.extentions.visible


@BindingAdapter("loading")
fun ErrorLoadingView.setLoading(isLoading: Boolean?) {
    this.isLoading = isLoading == true
}

@BindingAdapter("exception")
fun ErrorLoadingView.setException(exception: Exception?) {
    handleError(exception)
}

@BindingAdapter("message")
fun ErrorLoadingView.setMessage(message: String?) {
    message?.let {
        showEmptyView(message)
    }
}

@BindingAdapter("noData")
fun ErrorLoadingView.setNoData(noData: Boolean) {
    if (noData)
        showEmptyView(context.getString(R.string.space_x_no_data_message))
}

@BindingAdapter("isVisible")
fun ErrorLoadingView.isVisible(isVisible: Boolean) {
    if (isVisible)
        visible()
    else gone()
}