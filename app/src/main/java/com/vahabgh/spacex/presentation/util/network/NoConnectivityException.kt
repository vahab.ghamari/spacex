package com.vahabgh.spacex.presentation.util.network

import java.io.IOException

class NoConnectivityException : IOException()

class InternalServerException : Exception()

class NotFoundException : Exception()

class ForbiddenException : Exception()

