package com.vahabgh.spacex.presentation.ui.paginversion

import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.vahabgh.core.domain.Launch
import com.vahabgh.spacex.R
import com.vahabgh.spacex.databinding.FragmentPagingLaunchesBinding
import com.vahabgh.spacex.presentation.ui.base.BaseFragment
import com.vahabgh.spacex.presentation.ui.paginversion.paigng.*
import dagger.hilt.android.AndroidEntryPoint


/**
 *
 * this fragment acts like [com.vahabgh.spacex.presentation.ui.pastlaunches.PastLaunchesFragment]
 * but it handles pagination with PagingLibrary of jetPack components
 *
 */
@AndroidEntryPoint
class PagingPastLaunchesFragment :
    BaseFragment<PagingPastLaunchesViewModel, FragmentPagingLaunchesBinding>() {

    override val layoutRes: Int
        get() = R.layout.fragment_paging_launches

    override val viewModel: PagingPastLaunchesViewModel by viewModels()

    private lateinit var adapter: PagingLaunchAdapter

    private var retry: (() -> Unit)? = null

    override fun config() {

        binding?.errorLoadingView?.setOnRetryClickListener {
            retry?.invoke()
        }

        binding?.ivBack?.setOnClickListener {
            activity?.onBackPressed()
        }

        binding?.rvPastLaunches?.addItemDecoration(
            DividerItemDecoration(
                context
                , LinearLayoutManager.VERTICAL
            )
        )

        setAdapter()

    }

    private fun setAdapter() {
        adapter = PagingLaunchAdapter(::onItemClick, ::retryDelegate)
        binding?.rvPastLaunches?.adapter = adapter
    }

    override fun bindObservables() {

        viewModel.launchList.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
        })

        viewModel.networkState.observe(viewLifecycleOwner, Observer { pagingState ->
            handlePagination(pagingState)

        })
    }


    private fun handlePagination(pagingState: PagingStateResponse) {

        retry = pagingState.retry

        when(pagingState.state) {
            InitialPageLoading ->
                viewModel.showProgress()
            InitialPageLoaded -> {
                viewModel.hideErrorLoadingView()
                adapter.setPagingState(PaginatingState)
            }
            is InitialPageFailed -> {
                viewModel.hideProgress()
                viewModel.showException(pagingState.state.exception)
            }
            PaginatingState,
            PagingDone ,
            is PagingError ->
                adapter.setPagingState(pagingState.state)
        }

    }

    /**
     *
     * it is the delegate when an item is clicked in recyclerView
     * @param[launch] is the clicked item by user
     *
     */
    private fun onItemClick(launch: Launch?) {
        launch?.missionName?.let { missionName ->
            PagingPastLaunchesFragmentDirections.actionPagingPastLaunchesFragmentToLaunchDetailFragment(
                missionName
            ).let {
                findNavController().navigate(it)
            }
        }
    }

    /**
     * retry delegate for fetching next pages from
     * [com.vahabgh.spacex.presentation.ui.pastlaunches.list.FooterViewHolder]
     * when error happens
     */
    private fun retryDelegate() {
        retry?.invoke()
    }

}