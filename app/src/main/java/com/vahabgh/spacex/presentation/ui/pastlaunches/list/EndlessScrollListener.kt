package com.vahabgh.spacex.presentation.ui.pastlaunches.list

import androidx.recyclerview.widget.RecyclerView


/**
 * notify others by [onLoadMore] when list reaches the end
 */
abstract class EndlessScrollListener : RecyclerView.OnScrollListener() {

    abstract fun onLoadMore()

    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)

        if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
            onLoadMore()
        }
    }
}