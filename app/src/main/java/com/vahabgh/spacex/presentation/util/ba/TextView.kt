package com.vahabgh.spacex.presentation.util.ba

import android.graphics.Typeface
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.vahabgh.spacex.R


@BindingAdapter("makeBold")
fun TextView.setBold(makeBold : Boolean?) {
    if (makeBold == true) {
        setTypeface(null,Typeface.BOLD)
    }
}