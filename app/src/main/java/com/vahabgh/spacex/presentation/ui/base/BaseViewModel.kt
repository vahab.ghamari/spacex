package com.vahabgh.spacex.presentation.ui.base

import android.os.Looper
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.lang.Exception

/**
 *
 * base viewModel which works with [BaseFragment] for handling common behaviours
 * of loading data and handling error for view
 *
 */
open class BaseViewModel : ViewModel() {


    private val _showLoading: MutableLiveData<Boolean> = MutableLiveData(true)
    val showLoading: LiveData<Boolean>
        get() = _showLoading

    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String>
        get() = _errorMessage

    private val _exception = MutableLiveData<Exception>()
    val exception: LiveData<Exception>
        get() = _exception


    private val _errorLoadingVisibility = MutableLiveData<Boolean>()
    val errorLoadingVisibility: LiveData<Boolean>
        get() = _errorLoadingVisibility

    private val _noData = MutableLiveData<Boolean>()
    val noData: LiveData<Boolean>
        get() = _noData

    open fun showProgress() {
        _showLoading.value = true
    }

    open fun hideProgress() {
        _showLoading.value = false
    }

    open fun showException(exception: Exception) {
        _exception.value = exception
    }

    open fun hideErrorLoadingView() {
        _errorLoadingVisibility.value = false
    }

    open fun showErrorLoadingView() {
        _errorLoadingVisibility.value = true
    }

    open fun setErrorMessage(message: String?) {
        if (message.isNullOrEmpty()) return
        _errorMessage.value = message
    }

    open fun showNoDataView() {
        _noData.value = true
    }

    open fun hideNoDataView() {
        _noData.value = false
    }
}
