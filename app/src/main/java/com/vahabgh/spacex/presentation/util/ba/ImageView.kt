package com.vahabgh.spacex.presentation.util.ba

import android.content.Context
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.transition.DrawableCrossFadeTransition
import com.vahabgh.spacex.R


@BindingAdapter("imageUrl")
fun ImageView.setImage(imageUrl : String?) {
    imageUrl?.let {
        Glide.with(this).
        load(imageUrl).
        placeholder(ContextCompat.getDrawable(context, R.drawable.spacex_place_holder)).
        transition(DrawableTransitionOptions.withCrossFade()).
        into(this)
    }
}

@BindingAdapter("imageResource")
fun ImageView.setImage(image : Int?) {
    image?.let {
        setImageDrawable(ContextCompat.getDrawable(context,image))
    }
}