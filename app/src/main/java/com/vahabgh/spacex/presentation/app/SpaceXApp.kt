package com.vahabgh.spacex.presentation.app

import androidx.multidex.MultiDexApplication

open class SpaceXApp : MultiDexApplication()