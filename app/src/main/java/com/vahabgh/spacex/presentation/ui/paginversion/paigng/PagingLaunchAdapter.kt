package com.vahabgh.spacex.presentation.ui.paginversion.paigng

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.vahabgh.core.domain.Launch
import com.vahabgh.spacex.R
import com.vahabgh.spacex.databinding.ViewHolderFooterLoadingBinding
import com.vahabgh.spacex.databinding.ViewHolderLaunchItemBinding
import com.vahabgh.spacex.presentation.ui.pastlaunches.PaginationError
import com.vahabgh.spacex.presentation.ui.pastlaunches.list.FooterViewHolder
import com.vahabgh.spacex.presentation.ui.pastlaunches.list.LaunchItemViewHolder

class PagingLaunchAdapter(
    private val onItemClick: (Launch?) -> Unit,
    private val retryDelegate: () -> Unit
) : PagedListAdapter<Launch, RecyclerView.ViewHolder>(LaunchDiffCallBack()) {


    private var pagingState: PagingState? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        if (viewType == R.layout.view_holder_footer_loading)
            return FooterViewHolder(
                ViewHolderFooterLoadingBinding.inflate(inflater, parent, false),
                retryDelegate
            )
        if (viewType == R.layout.view_holder_launch_item)
            return LaunchItemViewHolder(
                ViewHolderLaunchItemBinding.inflate(inflater, parent, false),
                onItemClick
            )
        throw IllegalArgumentException("There is no viewHolder match this type")
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LaunchItemViewHolder)
            holder.bind(getItem(position))

        if (holder is FooterViewHolder)
            holder.bind(pagingState)
    }

    private fun hasFooterItem(): Boolean {
        return pagingState != null && (pagingState == PaginatingState
                || pagingState is PagingError)
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasFooterItem()) 1 else 0
    }

    override fun getItemViewType(position: Int): Int {
        if (hasFooterItem() && position == itemCount - 1)
            return R.layout.view_holder_footer_loading
        return R.layout.view_holder_launch_item
    }


    /**
     * thi functions handles different states of pagination for showing
     * right view of [FooterViewHolder]
     *
     * @param [com.vahabgh.spacex.presentation.ui.paginversion.paigng.PagingState] set state of pagination
     *
     * 1-loading [PaginatingState]
     * 2-show error [PagingError]
     * 3-removing it from recyclerview when pagination is done [PagingDone]
     *
     */
    fun setPagingState(newPagingState: PagingState) {

        val hadFooter = hasFooterItem()
        this.pagingState = newPagingState

        if (newPagingState is PagingDone) {
            if (hadFooter)
                notifyItemRemoved(super.getItemCount())
        } else {

            if (newPagingState is PaginatingState) {
                if (!hadFooter)
                    notifyItemInserted(super.getItemCount())
                else notifyItemChanged(itemCount - 1)
            }

            if (newPagingState is PagingError)
                if (hadFooter)
                    notifyItemChanged(itemCount - 1)

        }
    }

    class LaunchDiffCallBack : DiffUtil.ItemCallback<Launch>() {
        override fun areItemsTheSame(oldItem: Launch, newItem: Launch): Boolean {
            return oldItem.missionName.trim() == newItem.missionName.trim()
        }

        override fun areContentsTheSame(oldItem: Launch, newItem: Launch): Boolean {
            return oldItem == newItem
        }
    }

}