package com.vahabgh.spacex.presentation.util.customview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.vahabgh.spacex.R
import com.vahabgh.spacex.databinding.NoDataViewBinding
import com.vahabgh.spacex.presentation.util.extentions.gone
import com.vahabgh.spacex.presentation.util.extentions.visible
import com.vahabgh.spacex.presentation.util.network.ForbiddenException
import com.vahabgh.spacex.presentation.util.network.InternalServerException
import com.vahabgh.spacex.presentation.util.network.NoConnectivityException
import com.vahabgh.spacex.presentation.util.network.NotFoundException
import kotlinx.android.synthetic.main.no_data_view.view.*
import java.net.SocketTimeoutException

class ErrorLoadingView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    var isLoading: Boolean = false
        set(value) {
            field = value
            handleLoadingState(value)
        }

    init {
        val inflater = LayoutInflater.from(context)
        NoDataViewBinding.inflate(inflater, this, true)
        setBackgroundColor(ContextCompat.getColor(context, R.color.colorBackground))
    }

    private fun handleLoadingState(loading: Boolean) {
        if (loading) {
            showLoadingView()
        } else {
            hideLoadingView()
        }
    }

    private fun hideLoadingView() {
        cv_progress.gone()
    }

    private fun hideEmptyView() {
        icon_value.gone()
        txt_value.gone()
        btn_submit.gone()
    }

    private fun showLoadingView() {
        visible()
        hideEmptyView()
        cv_progress.visible()
    }

    fun handleError(exception: Exception?) {

        if (exception == null)
            return

        when (exception) {
            is NoConnectivityException -> {
                showNetworkConnectionError()
            }

            is SocketTimeoutException -> {
                showNetworkConnectionError()
            }

            is ForbiddenException,
            is InternalServerException -> {
                showServerError()
            }

            is NotFoundException -> {
                showEmptyView(context.getString(R.string.space_x_no_data_message))
            }

            else -> {
                showEmptyView(context.getString(R.string.space_x_failed_operation_message))
            }
        }

    }

    fun showEmptyView(message: String) {
        showMessageOrErrorWithoutRetry()
        icon_value.setImageDrawable(
            ContextCompat.getDrawable(context, R.drawable.ic_error_general)
        )
        txt_value.text = message
    }

    private fun showNetworkConnectionError() {
        showMessageOrErrorWithRetry()
        icon_value.setImageDrawable(
            ContextCompat.getDrawable(context, R.drawable.ic_error_connect)
        )
        txt_value.text = context.getString(R.string.space_x_connection_issue)
    }

    private fun showServerError() {
        showMessageOrErrorWithRetry()
        icon_value.setImageDrawable(
            ContextCompat.getDrawable(context, R.drawable.ic_error_server)
        )
        txt_value.text = context.getString(R.string.space_x_server_issue)
    }

    fun setOnRetryClickListener(listener: () -> Unit) {
        btn_submit?.setOnClickListener {
            listener()
            handleLoadingState(true)
            hideEmptyView()
        }
    }

    private fun showMessageOrErrorWithRetry() {
        visible()
        icon_value.visible()
        txt_value.visible()
        btn_submit.visible()
    }

    private fun showMessageOrErrorWithoutRetry() {
        visible()
        icon_value.visible()
        txt_value.visible()
        btn_submit.gone()
    }
}