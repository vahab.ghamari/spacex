package com.vahabgh.spacex.presentation.di

import android.content.Context
import androidx.room.Room
import com.vahabgh.spacex.framework.db.LaunchesDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideLaunchDatabase(@ApplicationContext applicationContext: Context): LaunchesDatabase {
        return Room.databaseBuilder(
            applicationContext,
            LaunchesDatabase::class.java, "LaunchesDB"
        ).build()
    }

}