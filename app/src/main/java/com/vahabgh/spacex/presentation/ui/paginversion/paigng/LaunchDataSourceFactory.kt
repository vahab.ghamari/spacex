package com.vahabgh.spacex.presentation.ui.paginversion.paigng

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.vahabgh.core.domain.Launch
import com.vahabgh.spacex.framework.pastlaunches.LaunchDataMapper
import com.vahabgh.spacex.framework.api.LaunchesService
import io.reactivex.disposables.CompositeDisposable

class LaunchDataSourceFactory constructor(
    private val launchesService: LaunchesService,
    private val compositeDisposable: CompositeDisposable,
    private val launchDataMapper: LaunchDataMapper

) : DataSource.Factory<Int, Launch>() {

    val launchLiveDataSource = MutableLiveData<LaunchDataSource>()

    override fun create(): DataSource<Int, Launch> {
        val launchDataSource =
            LaunchDataSource(launchesService, compositeDisposable, launchDataMapper)
        launchLiveDataSource.postValue(launchDataSource)
        return launchDataSource
    }

}