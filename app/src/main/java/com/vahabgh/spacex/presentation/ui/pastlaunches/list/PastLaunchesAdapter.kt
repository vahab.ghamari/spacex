package com.vahabgh.spacex.presentation.ui.pastlaunches.list

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.vahabgh.core.domain.Launch
import com.vahabgh.spacex.R
import com.vahabgh.spacex.databinding.ViewHolderFooterLoadingBinding
import com.vahabgh.spacex.databinding.ViewHolderLaunchItemBinding

class PastLaunchesAdapter(
    private val items: MutableList<Any>,
    private val onItemClick: (Launch?) -> Unit,
    private val retryDelegate: () -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        if (viewType == R.layout.view_holder_footer_loading)
            return FooterViewHolder(
                ViewHolderFooterLoadingBinding.inflate(inflater, parent, false),
                retryDelegate
            )
        if (viewType == R.layout.view_holder_launch_item)
            return LaunchItemViewHolder(
                ViewHolderLaunchItemBinding.inflate(inflater, parent, false),
                onItemClick
            )
        throw IllegalArgumentException("There is no viewHolder match this type")
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is FooterViewHolder)
            holder.bind(items[position] as FooterLoading)
        if (holder is LaunchItemViewHolder)
            holder.bind(items[position] as Launch)
    }

    override fun getItemViewType(position: Int): Int {
        return when (items[position]) {
            is FooterLoading -> R.layout.view_holder_footer_loading
            is Launch -> R.layout.view_holder_launch_item
            else -> throw IllegalArgumentException("There is no match type viewHolder with this item position")
        }
    }

    fun addFooter() {
        if (items.last() !is FooterLoading) {
            items.add(FooterLoading(false))
            notifyItemInserted(items.lastIndex)
        }
    }

    fun removeFooter() {
        if (items.last() is FooterLoading) {
            val lastIndex = items.lastIndex
            items.removeAt(lastIndex)
            notifyItemRemoved(lastIndex)
        }
    }

    fun showErrorFooter(errorMessage: String) {
        if (items.last() is FooterLoading) {
            val lastItem = items.last() as FooterLoading
            lastItem.hasError = true
            lastItem.errMessage = errorMessage
            notifyItemChanged(items.lastIndex)
        }
    }

    fun hideFooterError() {
        if (items.last() is FooterLoading) {
            val lastItem = items.last() as FooterLoading
            lastItem.hasError = false
            lastItem.errMessage = ""
            notifyItemChanged(items.lastIndex)
        }
    }


    fun updateData(newItems: MutableList<Any>) {
        if (newItems.isNotEmpty()) {
            val lastIndex = items.lastIndex
            items.addAll(lastIndex, newItems)
            notifyItemRangeInserted(lastIndex, newItems.size)
        }
    }
}