package com.vahabgh.spacex.presentation.di

import com.vahabgh.core.data.PastLaunchDataSource
import com.vahabgh.core.data.PastLaunchRepository
import com.vahabgh.core.interactors.GetPasLaunchesUseCase
import com.vahabgh.spacex.framework.pastlaunches.LaunchDataMapper
import com.vahabgh.spacex.framework.pastlaunches.PastLaunchInteractors
import com.vahabgh.spacex.framework.pastlaunches.PastLaunchesDataSourceImpl
import com.vahabgh.spacex.framework.api.LaunchesService
import com.vahabgh.spacex.framework.db.LaunchesDatabase
import com.vahabgh.spacex.presentation.ui.paginversion.paigng.PagingPastLaunchRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped

@Module
@InstallIn(ActivityRetainedComponent::class)
object PastLaunchesModule {

    @Provides
    @ActivityRetainedScoped
    fun providePagingRepository(launchesService: LaunchesService,mapper: LaunchDataMapper)
            : PagingPastLaunchRepository {
        return PagingPastLaunchRepository(launchesService,mapper)
    }

    @Provides
    @ActivityRetainedScoped
    fun providePastLaunchesDataMapper(): LaunchDataMapper {
        return LaunchDataMapper()
    }

    @Provides
    @ActivityRetainedScoped
    fun providePastLaunchesDataSource(launchesService: LaunchesService,
                                      launchesDatabase: LaunchesDatabase,
                                      launchDataMapper: LaunchDataMapper
    )
            : PastLaunchDataSource {
        return PastLaunchesDataSourceImpl(
            launchesService,
            launchesDatabase,
            launchDataMapper
        )
    }

    @Provides
    @ActivityRetainedScoped
    fun providePastLaunchesRepository(pastLaunchDataSource: PastLaunchDataSource): PastLaunchRepository {
        return PastLaunchRepository(pastLaunchDataSource)
    }

    @Provides
    @ActivityRetainedScoped
    fun provideGetPastLaunchesUseCase(repository: PastLaunchRepository): GetPasLaunchesUseCase {
        return GetPasLaunchesUseCase(repository)
    }

    @Provides
    @ActivityRetainedScoped
    fun providePastLaunchInteractors(
        getPasLaunchesUseCase: GetPasLaunchesUseCase
    ): PastLaunchInteractors {
        return PastLaunchInteractors(
            getPasLaunchesUseCase
        )
    }

}