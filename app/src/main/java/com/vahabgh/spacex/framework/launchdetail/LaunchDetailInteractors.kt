package com.vahabgh.spacex.framework.launchdetail

import com.vahabgh.core.interactors.GetLaunchDetailByNameUseCase

class LaunchDetailInteractors(
    val getLaunchDetailByNameUseCase: GetLaunchDetailByNameUseCase
)