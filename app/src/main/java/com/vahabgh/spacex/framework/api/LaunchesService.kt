package com.vahabgh.spacex.framework.api

import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface LaunchesService {

    @GET("launches")
    suspend fun getPastLaunches(@Query("limit") limit: Int, @Query("offset") offset: Int) : Response<List<LaunchApi>>

    @GET("launches")
    fun getSinglePastLaunches(@Query("limit") limit: Int, @Query("offset") offset: Int) : Single<List<LaunchApi>>
}