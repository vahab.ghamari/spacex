package com.vahabgh.spacex.framework.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
data class LaunchEntity(
    @ColumnInfo(name = "mission_name") val missionName: String?,
    @ColumnInfo(name = "flight_number") val flightNumber: Int?,
    @ColumnInfo(name = "mission_date") val missionDate: String?,
    @ColumnInfo(name = "launchYear") val launchYear: String?,
    @ColumnInfo(name = "detail") val detail: String?,
    @ColumnInfo(name = "rocket_name") val rocketName: String?,
    @ColumnInfo(name = "rocket_id") val rocketId: String?,
    @ColumnInfo(name = "small_image") val smallMissionImage: String?,
    @ColumnInfo(name = "large_image") val largeMissionImage: String?,
    @ColumnInfo(name = "wiki_link") val wikiLink: String?,
    @ColumnInfo(name = "youtube_link") val youtubeLink: String?,
    @ColumnInfo(name = "youtube_id") val youtubeId: String?,
    @ColumnInfo(name = "article") val article: String?,
    @ColumnInfo(name = "successful") val successful: Boolean?,
    @ColumnInfo(name = "page_index") val pageIndex: Int,
    @PrimaryKey(autoGenerate = true) val id: Int = 0
)
