package com.vahabgh.spacex.framework.launchdetail

import com.vahabgh.core.data.LaunchDetailDataSource
import com.vahabgh.core.data.ResponseData
import com.vahabgh.core.domain.LaunchDetail
import com.vahabgh.spacex.framework.db.LaunchesDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class LaunchDetailDataSourceImpl(
    private val database: LaunchesDatabase,
    private val mapper: LaunchDetailDataMapper
) : LaunchDetailDataSource{

    override suspend fun getLaunchesByName(name: String): Flow<ResponseData<LaunchDetail>> {

        return flow {
            val launch = database.launchesDao().getByMissionName(name)
            mapper.mapFromEntity(launch).apply {
                emit(ResponseData.success(this))
            }
        }.flowOn(Dispatchers.IO)

    }
}