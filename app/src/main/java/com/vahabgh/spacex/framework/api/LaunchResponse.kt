package com.vahabgh.spacex.framework.api

import com.google.gson.annotations.SerializedName
import com.vahabgh.core.domain.Links
import com.vahabgh.core.domain.Rocket

data class LaunchApi(

    @SerializedName("flight_number")
    val flightNumber: Int?,

    @SerializedName("mission_name")
    val missionName: String?,

    @SerializedName("launch_date_utc")
    val missionDate: String?,

    @SerializedName("launch_year")
    val launchYear: String?,

    @SerializedName("launch_success")
    val launchSuccess: Boolean?,

    @SerializedName("details")
    val detail: String?,

    @SerializedName("rocket")
    val rocket: RocketApi?,

    @SerializedName("links")
    val links: LinksApi?
)


data class LinksApi(

    @SerializedName("mission_patch_small")
    val smallMissionImage: String?,

    @SerializedName("mission_patch")
    val largeMissionImage: String?,

    @SerializedName("wikipedia")
    val wikiLink: String?,

    @SerializedName("video_link")
    val youtubeLink: String?,

    @SerializedName("article_link")
    val article: String?,

    @SerializedName("youtube_id")
    val youtubeId: String?

)

data class RocketApi(
    @SerializedName("rocket_id")
    val rocketId: String?,

    @SerializedName("rocket_name")
    val rocketName: String?
)

