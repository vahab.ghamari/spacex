package com.vahabgh.spacex.framework.pastlaunches

import com.vahabgh.core.interactors.GetPasLaunchesUseCase

class PastLaunchInteractors(
    val getPasLaunchesUseCase: GetPasLaunchesUseCase
)