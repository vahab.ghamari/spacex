package com.vahabgh.spacex.framework.pastlaunches

import android.annotation.SuppressLint
import com.vahabgh.core.domain.Launch
import com.vahabgh.core.domain.Links
import com.vahabgh.core.domain.Rocket
import com.vahabgh.spacex.framework.api.LaunchApi
import com.vahabgh.spacex.framework.api.LinksApi
import com.vahabgh.spacex.framework.api.RocketApi
import com.vahabgh.spacex.framework.db.LaunchEntity
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class LaunchDataMapper {

    fun mapToEntities(launchApis: List<LaunchApi>): List<LaunchEntity> {
        return launchApis.map {launchApi ->
            LaunchEntity(
                launchApi.missionName?:"",
                launchApi.flightNumber?:-1,
                launchApi.missionDate,
                launchApi.launchYear,
                launchApi.detail,
                launchApi.rocket?.rocketName,
                launchApi.rocket?.rocketId,
                launchApi.links?.smallMissionImage,
                launchApi.links?.largeMissionImage,
                launchApi.links?.wikiLink,
                launchApi.links?.youtubeLink,
                launchApi.links?.youtubeId,
                launchApi.links?.article,launchApi.launchSuccess,0
            )
        }
    }

    fun mapFromEntity(launchEntities: List<LaunchEntity>): List<Launch> {
        return launchEntities.map { launchEntity ->
            Launch(
                "",
                launchEntity.flightNumber ?: -1,
                launchEntity.missionName ?: "-",
                convertDate(launchEntity.missionDate),
                launchEntity.launchYear ?: "-",
                launchEntity.detail ?: "-",
                mapRocket(launchEntity.rocketId,launchEntity.rocketName),
                mapLink(
                    launchEntity.smallMissionImage,
                    launchEntity.largeMissionImage,
                    launchEntity.wikiLink,
                    launchEntity.youtubeLink,
                    launchEntity.article,
                    launchEntity.youtubeId
                ),launchEntity.successful?:false
            )
        }
    }


    fun map(launchApis: List<LaunchApi>): List<Launch> {
        return launchApis.map { launchApi ->
            Launch(
                "",
                launchApi.flightNumber ?: -1,
                launchApi.missionName ?: "-",
                convertDate(launchApi.missionDate),
                launchApi.launchYear ?: "-",
                launchApi.detail ?: "-",
                mapRocket(launchApi.rocket),
                mapLink(launchApi.links),launchApi.launchSuccess ?: false
            )
        }
    }


    private fun mapRocket(
        rocketId: String?,
        rocketName: String?
    ): Rocket? {
        return Rocket(
            rocketId,
            rocketName
        )

    }


    private fun mapRocket(rocketApi: RocketApi?): Rocket? {
        return rocketApi?.let {
            Rocket(
                it.rocketId,
                it.rocketName
            )
        }
    }

    private fun mapLink(
        smallImage: String?,
        largeImage: String?,
        wikiLink: String?,
        youtubeLink: String?,
        article: String?,
        youtubeId: String?
    ): Links? {
        return Links(
            smallImage,
            largeImage,
            wikiLink,
            youtubeLink,
            article,
            youtubeId
        )
    }

    private fun mapLink(linksApi: LinksApi?): Links? {
        return linksApi?.let {
            Links(
                it.smallMissionImage,
                it.largeMissionImage,
                it.wikiLink,
                it.youtubeLink,
                it.article,
                it.youtubeId
            )
        }
    }

    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    @SuppressLint("SimpleDateFormat")
    private fun convertDate(dateTime : String?) : String {
        dateTime?:return ""
        val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
        val date: Date = dateFormat.parse(dateTime)
        val formatter: DateFormat = SimpleDateFormat("yyyy-MM-dd")
        return formatter.format(date)
    }

}