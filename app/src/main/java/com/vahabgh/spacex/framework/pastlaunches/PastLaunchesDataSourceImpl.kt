package com.vahabgh.spacex.framework.pastlaunches

import com.vahabgh.core.data.PastLaunchDataSource
import com.vahabgh.core.data.ResponseData
import com.vahabgh.core.domain.Launch
import com.vahabgh.spacex.framework.api.LaunchApi
import com.vahabgh.spacex.framework.api.LaunchesService
import com.vahabgh.spacex.framework.db.LaunchesDatabase
import com.vahabgh.spacex.presentation.util.network.ForbiddenException
import com.vahabgh.spacex.presentation.util.network.InternalServerException
import com.vahabgh.spacex.presentation.util.network.NoConnectivityException
import com.vahabgh.spacex.presentation.util.network.NotFoundException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import java.lang.Exception

class PastLaunchesDataSourceImpl(
    private val pastLaunchesService: LaunchesService,
    private val database: LaunchesDatabase,
    private val mapper: LaunchDataMapper
) : PastLaunchDataSource {

    override suspend fun getPastLaunches(
        limit: Int,
        offset: Int
    ): Flow<ResponseData<List<Launch>>> {

        return flow {
            try {

                val response = pastLaunchesService.getPastLaunches(limit, offset)

                if (response.isSuccessful) {

                    if (response.body().isNullOrEmpty()){
                        emit(ResponseData.success(emptyList()))
                        return@flow
                    }

                    cacheData(response.body()!!)
                    emit(
                        ResponseData.success(
                            mapper.map(response.body()!!)
                        )
                    )

                } else {
                    if(!readFromCache(this, limit, offset))
                        emit(handleException(response.code()))
                }

            } catch (exception : Exception) {

                if (!readFromCache(this, limit, offset)) {
                    emit(ResponseData.error(exception))
                }

            }

        }.flowOn(Dispatchers.IO)
    }

    private fun cacheData(launchApis: List<LaunchApi>) {
        database.launchesDao().insertAll(
            mapper.mapToEntities(launchApis)
        )
    }

    private fun handleException(code: Int): ResponseData<List<Launch>> {

        if (code == 403)
            return ResponseData.error(ForbiddenException())


        if (code == 404)
            return ResponseData.error(NotFoundException())


        if (code == 500)
            return ResponseData.error(InternalServerException())

        return ResponseData.error(Exception())

    }

    private suspend fun readFromCache(
        flowCollector: FlowCollector<ResponseData<List<Launch>>>,
        limit: Int,
        offset: Int
    ): Boolean {
        val cacheData = database.launchesDao().getAll(limit, offset)
        return if (!cacheData.isNullOrEmpty()) {
            flowCollector.emit(
                ResponseData.success(
                    mapper.mapFromEntity(cacheData)
                )
            )
            true
        } else {
            false
        }
    }

}