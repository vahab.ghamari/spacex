package com.vahabgh.spacex.framework.db

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [LaunchEntity::class], version = 1)
abstract class LaunchesDatabase : RoomDatabase() {
    abstract fun launchesDao(): LaunchesDao
}