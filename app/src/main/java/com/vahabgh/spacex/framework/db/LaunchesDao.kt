package com.vahabgh.spacex.framework.db

import androidx.room.*

@Dao
interface LaunchesDao {

    @Query("SELECT * FROM LaunchEntity")
    fun getAll(): List<LaunchEntity>

    @Query("SELECT * FROM LaunchEntity  LIMIT :limit OFFSET :offset")
    fun getAll(limit : Int,offset : Int): List<LaunchEntity>

    @Query("SELECT * FROM LaunchEntity where page_index=:pageIndex")
    fun getAllByPageIndex(pageIndex: Int): List<LaunchEntity>

    @Query("SELECT * FROM LaunchEntity where mission_name=:missionName")
    fun getByMissionName(missionName: String): LaunchEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(launchEntity: LaunchEntity)

    @Transaction
    fun insertAll(entities: List<LaunchEntity>) {
        entities.forEach {
            insert(it)
        }
    }


}