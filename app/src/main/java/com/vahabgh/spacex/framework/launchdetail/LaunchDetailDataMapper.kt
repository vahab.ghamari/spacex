package com.vahabgh.spacex.framework.launchdetail

import android.annotation.SuppressLint
import android.content.Context
import com.vahabgh.core.domain.*
import com.vahabgh.spacex.R
import com.vahabgh.spacex.framework.db.LaunchEntity
import dagger.hilt.android.qualifiers.ApplicationContext
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class LaunchDetailDataMapper(@ApplicationContext val context: Context) {


    fun mapFromEntity(launchEntity: LaunchEntity): LaunchDetail {
        return LaunchDetail(
            launchEntity.missionName ?: "-",
            launchEntity.youtubeId ?: "",
            createLaunchDetail(launchEntity)
        )
    }

    private fun createLaunchDetail(launchEntity: LaunchEntity): List<DetailItem> {
        val launchDetails = mutableListOf<DetailItem>()

        if (!launchEntity.missionName.isNullOrEmpty())
            launchDetails.add(HeaderItem(launchEntity.missionName,
                convertDate(launchEntity.missionDate)))

        if (!launchEntity.detail.isNullOrEmpty())
            launchDetails.add(DescriptionItem(launchEntity.detail))

        if (!launchEntity.rocketName.isNullOrEmpty())
            launchDetails.add(
                InfoItem(R.drawable.ic_rocket, context.getString(R.string.space_x_rocket_name),
                    launchEntity.rocketName
                )
            )

        if (!launchEntity.launchYear.isNullOrEmpty())
            launchDetails.add(
                InfoItem(
                    R.drawable.ic_launch_year,  context.getString(R.string.space_x_launch_year), launchEntity.launchYear
                )
            )

        if (launchEntity.flightNumber.toString().isNotEmpty())
            launchDetails.add(
                InfoItem(
                    R.drawable.ic_flight,  context.getString(R.string.space_x_flight_number), launchEntity.flightNumber.toString()
                )
            )

        if (!launchEntity.wikiLink.isNullOrEmpty())
            launchDetails.add(
                InfoItem(
                    R.drawable.ic_link,  context.getString(R.string.space_x_wiki_link), launchEntity.wikiLink
                )
            )

        if (!launchEntity.article.isNullOrEmpty())
            launchDetails.add(
                InfoItem(
                    R.drawable.ic_link,  context.getString(R.string.space_x_related_article), launchEntity.article
                )
            )

        return launchDetails
    }

    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    @SuppressLint("SimpleDateFormat")
    private fun convertDate(dateTime : String?) : String {
        dateTime?:return ""
        val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
        val date: Date = dateFormat.parse(dateTime)
        val formatter: DateFormat = SimpleDateFormat("yyyy-MM-dd")
        return formatter.format(date)
    }

}