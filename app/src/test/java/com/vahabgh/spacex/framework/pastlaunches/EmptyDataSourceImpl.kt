package com.vahabgh.spacex.framework.pastlaunches

import com.vahabgh.core.data.PastLaunchDataSource
import com.vahabgh.core.data.ResponseData
import com.vahabgh.core.domain.Launch
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class EmptyDataSourceImpl : PastLaunchDataSource {

    override suspend fun getPastLaunches(
        limit: Int,
        offset: Int
    ): Flow<ResponseData<List<Launch>>> {
        return flow {
            emit(ResponseData.success(emptyList()))
        }
    }
}