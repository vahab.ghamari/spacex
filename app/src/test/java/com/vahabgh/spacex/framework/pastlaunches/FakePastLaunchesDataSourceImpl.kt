package com.vahabgh.spacex.framework.pastlaunches

import com.vahabgh.core.data.PastLaunchDataSource
import com.vahabgh.core.data.ResponseData
import com.vahabgh.core.domain.Launch
import com.vahabgh.core.domain.Links
import com.vahabgh.core.domain.Rocket
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class FakePastLaunchesDataSourceImpl : PastLaunchDataSource {


    private val launches = mutableListOf<Launch>()


    init {

        repeat(100) { index ->
            launches.add(

                Launch(
                    index.toString(),
                    index,
                    "missionName_$index",
                    "missionDate$index",
                    "launchYear$index",
                    "launchDetail$index",
                    Rocket(
                        "rocketId$index",
                        "rocketName$index"
                    ),
                    Links(
                        "smallMissionImage$index.png",
                        "lImage$index.jpeg", "wikiLink$index",
                        "youtubeLink$index", "article$index", "youtubeId"
                    ),
                    index % 2 == 0
                )

            )
        }

    }


    override suspend fun getPastLaunches(
        limit: Int,
        offset: Int
    ): Flow<ResponseData<List<Launch>>> {
        return flow {

            if (offset < 0) {
                emit(ResponseData.error(Exception()))
                return@flow
            }

            if (offset == 0) {
                emit(ResponseData.success(launches.take(limit)))
                return@flow
            }

            val toIndex = offset + limit
            val fromIndex = offset

            if (toIndex >= launches.size) {
                val lastItemCounts = launches.size - offset
                if (lastItemCounts>0)
                    emit(ResponseData.success(launches.takeLast(lastItemCounts)))
                else emit(ResponseData.error(Exception()))
            }


            val subList = launches.subList(offset, toIndex).toList()
            emit(ResponseData.success(subList))

        }
    }
}