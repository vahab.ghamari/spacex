package com.vahabgh.spacex.framework.launchdetail

import com.vahabgh.core.data.LaunchDetailDataSource
import com.vahabgh.core.data.ResponseData
import com.vahabgh.core.domain.LaunchDetail
import com.vahabgh.spacex.framework.db.LaunchEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.lang.Exception

class FakeLaunchDetailDataSourceImpl(val mapper: LaunchDetailDataMapper) : LaunchDetailDataSource {

    private val launchesStorage = mutableListOf<LaunchEntity>()


    init {

        repeat(20) { index ->
            launchesStorage.add(
                LaunchEntity(
                    "missionName_$index",
                    index, "missionDate",
                    "launchYear$index",
                    "launchDetail$index",
                    "rocketNAme", "rocketId", "sImage", "limage"
                    , "wikilink", "youtube", "youtubeId", "article",
                    true, 0, 0
                )
            )
        }

    }


    override suspend fun getLaunchesByName(name: String): Flow<ResponseData<LaunchDetail>> {
        return flow {

            launchesStorage.forEach {
                if (it.missionName == name){
                    emit(ResponseData.success(mapper.mapFromEntity(it)))
                    return@flow
                }
            }

            emit(ResponseData.error(Exception()))
        }
    }

}