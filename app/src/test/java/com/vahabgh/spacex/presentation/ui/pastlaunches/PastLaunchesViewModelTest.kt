package com.vahabgh.spacex.presentation.ui.pastlaunches

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.vahabgh.core.data.PastLaunchRepository
import com.vahabgh.core.interactors.GetPasLaunchesUseCase
import com.vahabgh.repoinfo.MainCoroutineRule
import com.vahabgh.repoinfo.getOrAwaitValueTest
import com.vahabgh.spacex.framework.pastlaunches.EmptyDataSourceImpl
import com.vahabgh.spacex.framework.pastlaunches.FakePastLaunchesDataSourceImpl
import com.vahabgh.spacex.framework.pastlaunches.PastLaunchInteractors
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class PastLaunchesViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule(TestCoroutineDispatcher())

    lateinit var viewModel: PastLaunchesViewModel

    lateinit var useCase: GetPasLaunchesUseCase


    lateinit var emptyUseCase: GetPasLaunchesUseCase

    lateinit var emptyViewModel: PastLaunchesViewModel

    @Before
    fun setup() {

        useCase = GetPasLaunchesUseCase(PastLaunchRepository(FakePastLaunchesDataSourceImpl()))
        viewModel = PastLaunchesViewModel(PastLaunchInteractors(useCase))

        emptyUseCase = GetPasLaunchesUseCase(PastLaunchRepository(EmptyDataSourceImpl()))
        emptyViewModel = PastLaunchesViewModel(PastLaunchInteractors(emptyUseCase))

    }

    @Test
    fun `get first page data`() {

        viewModel.getFirstPage()

        val result = viewModel.pastLaunches.getOrAwaitValueTest()

        assertThat(result.size).isEqualTo(viewModel.limit)
    }

    @Test
    fun `if first page fetched successful no ErrorLoadingViewShould not be visible`() {

        viewModel.getFirstPage()

        val result = viewModel.pastLaunches.getOrAwaitValueTest()

        val loading = viewModel.showLoading.getOrAwaitValueTest()

        val loadingViewVisibility = viewModel.errorLoadingVisibility.getOrAwaitValueTest()

        if (!result.isNullOrEmpty()) { // data is not empty here
            assertThat(loading).isEqualTo(false)
            assertThat(loadingViewVisibility).isNotEqualTo(true)
        }
    }

    @Test
    fun `if first page fetched successful , state of pagination should be Paginating`() {

        viewModel.getFirstPage()

        val result = viewModel.pastLaunches.getOrAwaitValueTest()

        val loading = viewModel.showLoading.getOrAwaitValueTest()

        val pagingState = viewModel.paginateState.getOrAwaitValueTest()

        if (!result.isNullOrEmpty()) { // data is not empty
            assertThat(loading).isEqualTo(false)
            assertThat(pagingState).isEqualTo(Paginating)
        }
    }

    @Test
    fun `getFirst page returns empty list and empty view is visible`() {

        emptyViewModel.getFirstPage()

        val noDataView = emptyViewModel.noData.getOrAwaitValueTest()

        val loading = emptyViewModel.showLoading.getOrAwaitValueTest()

        assertThat(loading).isEqualTo(false)
        assertThat(noDataView).isEqualTo(true)

    }


    @After
    fun tearDown() {

    }
}