package com.vahabgh.spacex.presentation.ui.launchedetail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth
import com.google.common.truth.Truth.assertThat
import com.vahabgh.core.data.LaunchDetailRepository
import com.vahabgh.core.interactors.GetLaunchDetailByNameUseCase
import com.vahabgh.repoinfo.MainCoroutineRule
import com.vahabgh.repoinfo.getOrAwaitValueTest
import com.vahabgh.spacex.framework.launchdetail.FakeLaunchDetailDataSourceImpl
import com.vahabgh.spacex.framework.launchdetail.LaunchDetailDataMapper
import com.vahabgh.spacex.framework.launchdetail.LaunchDetailInteractors
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class LaunchDetailViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule(TestCoroutineDispatcher())

    lateinit var viewModel: LaunchDetailViewModel

    lateinit var useCase: GetLaunchDetailByNameUseCase

    @Before
    fun setup() {

        useCase = GetLaunchDetailByNameUseCase(
            LaunchDetailRepository(FakeLaunchDetailDataSourceImpl(
                LaunchDetailDataMapper()
            ))
        )
        viewModel = LaunchDetailViewModel(LaunchDetailInteractors(useCase))

    }


    @Test
    fun `get launch detail data with an existing name`() {

        val missionName = "missionName_3"

        viewModel.getData(missionName)

        val result = viewModel.launch.getOrAwaitValueTest()

        assertThat(result.title).isEqualTo(missionName)
    }

    @Test
    fun `get launch detail data with a none existing name must show empty view`() {

        val missionName = "missionName_78"

        viewModel.getData(missionName)

        val noData = viewModel.noData.getOrAwaitValueTest()

        assertThat(noData).isEqualTo(true)
    }

}