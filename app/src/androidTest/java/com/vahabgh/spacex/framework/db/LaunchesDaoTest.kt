package com.vahabgh.spacex.framework.db

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@SmallTest
class LaunchesDaoTest {

    private lateinit var database: LaunchesDatabase

    private lateinit var dao: LaunchesDao

    @Before
    fun setup() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            LaunchesDatabase::class.java
        )
            .allowMainThreadQueries()
            .build()

        dao = database.launchesDao()

    }

    @After
    fun tearDown() {
        database.close()
    }

    @Test
    fun insertLaunchEntityTest() {

        val missionName = "TestMission"

        val launchEntity = LaunchEntity(
            missionName,
            1,
            "missionDate",
            "launchYear",
            "detail",
            "rocketName",
            "rocketId",
            "smallMissionImage",
            "largeMissionImage",
            "wikiLink",
            "youtubeLink",
            "youtubeId",
            "article", true, 0
        )

        dao.insert(launchEntity)

        val launchEntityFromDb = dao.getAll()

        assertThat(launchEntityFromDb.map { it.missionName })
            .contains(launchEntity.missionName)

    }

    @Test
    fun insertMultipleLaunchEntityTest() {

        val insertItemCount = 5

        val launches = mutableListOf<LaunchEntity>()

        repeat(insertItemCount) {

            launches.add(
                LaunchEntity(
                    "TestMission",
                    1,
                    "missionDate",
                    "launchYear",
                    "detail",
                    "rocketName",
                    "rocketId",
                    "smallMissionImage",
                    "largeMissionImage",
                    "wikiLink",
                    "youtubeLink",
                    "youtubeId",
                    "article", true, 0
                )
            )

        }

        dao.insertAll(launches)

        val launchEntityFromDb = dao.getAll()

        assertThat(launchEntityFromDb.size)
            .isEqualTo(insertItemCount)

    }

    @Test
    fun getLaunchByNameTest() {

        val missionName = "TestMission"

        val launchEntity = LaunchEntity(
            missionName,
            1,
            "missionDate",
            "launchYear",
            "detail",
            "rocketName",
            "rocketId",
            "smallMissionImage",
            "largeMissionImage",
            "wikiLink",
            "youtubeLink",
            "youtubeId",
            "article", true, 0
        )

        dao.insert(launchEntity)

        val launchEntityFromDb = dao.getByMissionName(missionName)

        assertThat(launchEntityFromDb.missionName)
            .isEqualTo(launchEntity.missionName)

    }

    @Test
    fun getLaunchDataByLimitAndOffsetTest() {

        val insertItemCount = 15

        val launches = mutableListOf<LaunchEntity>()

        repeat(insertItemCount) {

            launches.add(
                LaunchEntity(
                    "TestMission",
                    1,
                    "missionDate",
                    "launchYear",
                    "detail",
                    "rocketName",
                    "rocketId",
                    "smallMissionImage",
                    "largeMissionImage",
                    "wikiLink",
                    "youtubeLink",
                    "youtubeId",
                    "article", true, 0
                )
            )

        }

        dao.insertAll(launches)

        val limit = 5
        var offset = 0


        val launchEntityFromDb = dao.getAll(limit,offset)

        assertThat(launchEntityFromDb.size)
            .isEqualTo(limit)
    }

    @Test
    fun getLaunchDataByLimitAndOffsetTest2() {

        val insertItemCount = 15

        val launches = mutableListOf<LaunchEntity>()

        repeat(insertItemCount) {

            launches.add(
                LaunchEntity(
                    "TestMission",
                    1,
                    "missionDate",
                    "launchYear",
                    "detail",
                    "rocketName",
                    "rocketId",
                    "smallMissionImage",
                    "largeMissionImage",
                    "wikiLink",
                    "youtubeLink",
                    "youtubeId",
                    "article", true, 0
                )
            )

        }

        dao.insertAll(launches)


        /**
         * by this limit and offset the query must return rest of data which is :
         * insertItemCount - offset
         */
        val limit = 5
        var offset = 13


        val launchEntityFromDb = dao.getAll(limit,offset)

        assertThat(launchEntityFromDb.size)
            .isEqualTo(insertItemCount - offset)
    }

}